provider "aws" {
  region = "${var.region}"
}

# fetch values generated from terraform vpc state
data "terraform_remote_state" "vpc" {
  backend = "local"
  config {  
    path = "${var.path_vpc_state}"
  }
}

module "db" {
  source                          = "terraform-aws-modules/rds-aurora/aws"
  name                            = "${var.rds_aurora_name}"
  username                        = "${var.rds_database_username}"
  password                        = "${var.rds_database_password}"
  database_name                   = "${var.rds_database_name}"

  engine                          = "aurora-mysql"
  engine_version                  = "5.7.12"

  vpc_id                          = "${data.terraform_remote_state.vpc.vpc_id}"
  subnets                         = ["${data.terraform_remote_state.vpc.database_subnets}"]
  port                            = "${var.rds_port_incoming}"

  replica_count                   = 1
  allowed_security_groups         = ["${data.terraform_remote_state.vpc.mysql_security_group_id}"]
  allowed_security_groups_count   = 1
  instance_type                   = "${var.db_instance_type}"
  storage_encrypted               = true
  apply_immediately               = true
  monitoring_interval             = 10
  db_parameter_group_name         = "${aws_db_parameter_group.aurora_db_57_parameter_group.id}"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.aurora_57_cluster_parameter_group.id}"

  tags                            = {
    Environment = "${var.env_tag}"
    Terraform   = "true"
  }
}

resource "aws_db_parameter_group" "aurora_db_57_parameter_group" {
  name        = "vader-aurora-db-57-parameter-group"
  family      = "aurora-mysql5.7"
  description = "vader-aurora-db-57-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_57_cluster_parameter_group" {
  name        = "vader-aurora-57-cluster-parameter-group"
  family      = "aurora-mysql5.7"
  description = "vader-aurora-57-cluster-parameter-group"
}
