variable "region" {
  description = "Region used on AWS"
  default     = "us-east-1"
}
variable "path_vpc_state" {
  description = "Local path where the tfstate of vpc resides"
  default     = "../vpc/terraform.tfstate"
}
variable "db_instance_type" {
  description = "Instance type which will be used for the database"
  default     = "db.t2.small"
}
variable "env_tag" {
  description = "Environment tag to be used"
  default     = "stage"
}
variable "rds_database_name" {
  description = "Database name to create automaticaly"
  default     = "redmine"
}
variable "rds_database_username" {
  description = "Username to use with the database"
  default     = "redmine"
}
variable "rds_database_password" {
  description = "Password of the user"
  default     = "8bTW2Xv2kmJGPF1Z"
}
variable "rds_aurora_name" {
  description = "Name which will have the aurora instance"
  default     = "vader-db-stage"
}
variable "rds_port_incoming" {
  description = "Port where the database will accept connections"
  default     = "3306"
}
