output "rds_database_name" {
  description = "Name for an automatically created database on cluster creation"
  value       = "${module.db.this_rds_cluster_database_name}"
}

output "rds_endpoint" {
  description = "Cluster endpoint"
  value       = "${module.db.this_rds_cluster_endpoint}"
}

output "rds_port" {
  description = "Port of the cluster"
  value       = "${module.db.this_rds_cluster_port}"
}

output "rds_master_password" {
  description = "The master password"
  value       = "${module.db.this_rds_cluster_master_password}"
}

output "rds_master_username" {
  description = "The master username"
  value       = "${module.db.this_rds_cluster_master_username}"
}
