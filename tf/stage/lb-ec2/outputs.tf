# ELB related
output "elb_dns_endpoint" {
  description = "DNS Name of the endpoint where the LB listens"
  value       = "${module.elb.this_elb_dns_name}"
}
output "elb_list_instances" {
  description = "List of instances on the elb"
  value       = ["${module.elb.this_elb_instances}"]
}
# EC2 related
output "ec2_private_ips" {
  description = "List of private IPs of app servers"
  value       = ["${module.ec2-instance.private_ip}"]
}
output "bastion_public_ip" {
  description = "Public IP of the bastion ssh host"
  value       = "${module.ec2-bastion.public_ip}"
}
output "bastion_security_group_id" {
  description = "Id of the bastion security group"
  value       = "${module.ec2-bastion.bastion_security_group_id}"
}
output "bastion_to_host_security_group_id" {
  description = "Id of the security group allowing ssh from bastion to host"
  value       = "${module.ec2-bastion.bastion_to_host_security_group_id}"
}
