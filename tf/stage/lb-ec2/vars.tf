variable "region" {
  description = "Region used on AWS"
  default     = "us-east-1"
}
variable "path_vpc_state" {
  description = "Local path where the tfstate of vpc resides"
  default     = "../vpc/terraform.tfstate"
}
variable "azs" {
  description = "Availablity Zones"
  default     = ["us-east-1a", "us-east-1b"]
}
variable "instance_type" {
  description = "Instance type which will be use for ec2"
  default     = "t2.micro"
}
variable "env_tag" {
  description = "Environment to be used with the tags"
  default     = "stage"
}
variable "key_name" {
  description = "Key name used for ssh"
  default     = "deployer-key"
}
variable "allowed_ssh_ips" {
  description = "List of IPs to be allowed to ssh-in"
  default     = ["167.62.56.35/32"]
}
variable "role_tag" {
  description = "Tag of the role wich will assume the instance"
  default     = "webapp"
}
variable "name_ec2" {
  description = "name which will be added to the instances"
  default     = "vader-stage"
}
variable "instance_port" {
  description = "The port the LB will connect"
  default     = "80"
}
variable "lb_port" {
  description = "The port where the LB will be listening"
  default     = "80"
}
variable "health_check_target" {
  description = "Target used by the LB for health checks"
  default     = "HTTP:80/"
}
variable "number_instances" {
  description = "Number of instances which will be deployed and added to the lb"
  default     = "2"
}
