provider "aws" {
  region = "${var.region}"
}

# Fetch last linux ami version to use on ec2
data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

# fetch values generated from terraform vpc state
data "terraform_remote_state" "vpc" {
  backend = "local"
  config {  
    path = "${var.path_vpc_state}"
  }
}

# provision a bastion host and his security groups allowing ssh from it to hosts on priv subnet
module "ec2-bastion" {
  source  = "../../modules/ec2-bastion"

  ami            = "${data.aws_ami.amazon_linux.id}"
  env_tag        = "${var.env_tag}"
  key_name       = "${var.key_name}"
  subnet_id      = "${element(data.terraform_remote_state.vpc.public_subnets,0)}"
  vpc_id         = "${data.terraform_remote_state.vpc.vpc_id}"
  allowed_ssh_in = ["${var.allowed_ssh_ips}"]
}

module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "1.14.0"

  instance_count              = "${var.number_instances}"
  name                        = "${var.name_ec2}"
  ami                         = "${data.aws_ami.amazon_linux.id}"
  instance_type               = "${var.instance_type}"
  vpc_security_group_ids      = ["${data.terraform_remote_state.vpc.lb_to_ec2-http-_security_group_id}","${module.ec2-bastion.bastion_to_host_security_group_id}"]
  subnet_id                   = "${element(data.terraform_remote_state.vpc.private_subnets,0)}"
  key_name                    = "${var.key_name}"
  
  ebs_block_device = [
    {
      device_name           = "/dev/xvdz"
      volume_type           = "gp2"
      volume_size           = "20"
      delete_on_termination = true
    },
  ]

  root_block_device = [
    {
      volume_size = "20"
      volume_type = "gp2"
    },
  ]

  tags = {
    Environment = "${var.env_tag}"
    Terraform   = "true"
    Role        = "${var.role_tag}"
  }

}

module "elb" {
  source = "terraform-aws-modules/elb/aws"
  version = "1.4.1"

  name = "elb-vader"

  subnets         = ["${data.terraform_remote_state.vpc.public_subnets}"]
  security_groups = ["${data.terraform_remote_state.vpc.http_80_security_group_id}"]
  internal        = false

  listener = [
    {
      instance_port     = "${var.instance_port}"
      instance_protocol = "HTTP"
      lb_port           = "${var.lb_port}"
      lb_protocol       = "HTTP"
    },
  ]

  health_check = [
    {
      target              = "${var.health_check_target}"
      interval            = 30
      healthy_threshold   = 2
      unhealthy_threshold = 2
      timeout             = 5
    },
  ]

  # ELB attachments
  number_of_instances = "${var.number_instances}"
  instances           = ["${module.ec2-instance.id}"]

  tags = {
    Environment = "${var.env_tag}"
    Terraform   = "true"
  }
}

