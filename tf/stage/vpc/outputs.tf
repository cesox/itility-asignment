# VPC 
output "vpc_id" {
  description = "The ID of the VPC"
  value       = "${module.vpc.vpc_id}"
}
output "azs" {
  description = "Availability zones deployed"
  value       = ["${module.vpc.azs}"]
}

# Subnets
output "private_subnets" {
  description = "List of private subnets"
  value       = ["${module.vpc.private_subnets}"]
}
output "database_subnets" {
  description = "List of database subnets"
  value       = ["${module.vpc.database_subnets}"]
}
output "public_subnets" {
  description = "List of public subnets"
  value       = ["${module.vpc.public_subnets}"]
}

# Security Groups
output "http_80_security_group_id" {
  description = "Id of the HTTP security group"
  value       = "${module.http_80_security_group.this_security_group_id}"
}
output "lb_to_ec2-http-_security_group_id" {
  description = "Id of the HTTP port 8080 security group"
  value       = "${module.security-group.this_security_group_id}"
}
output "mysql_security_group_id" {
  description = "Id of the MySQL security group"
  value       = "${module.security-group_mysql.this_security_group_id}"
}
output "key_name" {
  description = "Name of the key"
  value       = "${aws_key_pair.deployer-key.key_name}"
}
