variable "region" {
  description = "Region to use"
  default     = "us-east-1"
}
variable "azs" {
  description = "Availablity Zones"
  default     = ["us-east-1a", "us-east-1b"]
}
variable "vpc_cidr" {
  description = "CIDR Block to be used on the VPC"
  default     = "192.168.0.0/16"
}
variable "private_subnets" {
  description = "List of CIDR private subnets"
  default     = ["192.168.10.0/24"]
}
variable "database_subnets" {
  description = "List of CIDR subnet for databases"
  default     = ["192.168.20.0/24", "192.168.21.0/24"]
}
variable "public_subnets" {
  description = "List of CIDR public subnets"
  default     = ["192.168.30.0/24", "192.168.31.0/24"]
}
variable "env_tag" {
  description = "Environment to be used on tags"
  default     = "stage"
}
variable "hash_public_key" {
  description = "Has of the public key to be used for ssh log-in"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC90cE4teAky+DWxMtKJDDcpiRD6UxKkdvcLRVLOkGBoHP3YgnNTgSiTOjKlPdXSPNiqKGwKa+aVUzp7/rIbpIjMc79Lou+9pdjqG2FLLyntX7jONkjV8iFzxqzFOyAftsxKd5n3WpAUjiTVSe8ex4tLiT1DLGG5SUHc6W7opymn5k9sESmLqk3WZETApEZek7bDjPn5MIzLuZfs6z4QEW1D+3wHhC0bPdUmYG1AC04DzVmNRwoAvKIPwSCWm/BpTr0xWS+1sRjQdppRWx+oPYKGacEWY0GTEZmYVfadE9P9pbCZJq31l9ziEfYuhmCKbb3cTo23DgcPP5HG9cqYaMEVt4QdZWM8klza3q4oDsVEcICcdmuCX1/CyvLkNXuB3in9AXsM6GO1jeXYnsfU+/WZL4dq+jLceXsTlC68ex449SBO11ivV6e7RSEKzjRX+2XeLCqTUIuRSbLg9IjeKriWBSr623wsL/6JoW8qmahQsNnAt615cDTsWA3kcNFSsOtAbh1O9JkJMg3ABSGv6jNAJycEXtRz5bgIJ/1f8clOEpALMBXgWID7ivTF/qnk8YjycYSn354CyCoCBmGMzkBhyX67ceaJb1nXAOHvTF9e5hiXjXd+b01Jvg9UMqLwDUrytuxk6k/ZNOauFrszeFpSi9Bpp0Z8mCQdrIXIuNLQQ== leandrolemos@UY-IT00541"
}
