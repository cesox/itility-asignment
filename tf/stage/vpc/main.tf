provider "aws" {
  region     = "${var.region}"
  access_key = "AKIAI67US4LU6PL5BPEA"
  secret_key = "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"
}

# Creation and provisioning of VPC, subnets and RTs
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.53.0"

  name = "stage_vpc"
  cidr = "${var.vpc_cidr}"

  azs              = ["${var.azs}"]
  private_subnets  = ["${var.private_subnets}"]
  database_subnets = ["${var.database_subnets}"]
  public_subnets   = ["${var.public_subnets}"]

  create_database_subnet_route_table = true

  enable_nat_gateway                 = true
  single_nat_gateway                 = true

  tags = {
    Terraform   = "true"
    Environment = "${var.env_tag}"
  }

  vpc_tags = {
    Name = "vpc-vader"
  }
}

# Creation an provisioning of Security Groups
module "http_80_security_group" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"
  version = "2.11.0"

  name                     = "lb_internet_access"
  vpc_id                   = "${module.vpc.vpc_id}"
  ingress_cidr_blocks      = ["0.0.0.0/0"]

  tags = {
    Environment = "${var.env_tag}"
    Terraform   = "true"
  }
}

module "security-group" {
  source = "terraform-aws-modules/security-group/aws"
  version = "2.11.0"

  name                     = "lb_to_ec2"
  vpc_id                   = "${module.vpc.vpc_id}"
  ingress_cidr_blocks      = ["${var.public_subnets}"]

  tags = {
    Environment = "${var.env_tag}"
    Terraform   = "true"
  }
}

module "security-group_mysql" {
  source  = "terraform-aws-modules/security-group/aws//modules/mysql"
  version = "2.11.0"

  name                     = "ec2_access"
  vpc_id                   = "${module.vpc.vpc_id}"
  ingress_cidr_blocks      = ["${var.private_subnets}"]

  tags = {
    Environment = "${var.env_tag}"
    Terraform   = "true"
  }
}

# aprovision key pair
resource "aws_key_pair" "deployer-key" {
  key_name   = "deployer-key"
  public_key = "${var.hash_public_key}"
}


