variable "vpc_id" {
  type        = "string"
  description = "ID of the VPC where the bastion will be deployed"
}
variable "allowed_ssh_in" {
  type        = "list"
  description = "List of ips allowed to do ssh in"
}
variable "ami" {
  type        = "string"
  description = "AMI id to be used with the bastion host"
}
variable "key_name" {
  type        = "string"
  description = "Name of the key to be used for ssh access"
}
variable "instance_type" {
  type        = "string"
  description = "Instance type to be used with the bastion host"
  default     = "t2.micro"
}
variable "subnet_id" {
  type        = "string"
  description = "Subnet id where the bastion host will be placed"
}
variable "env_tag" {
  type        = "string"
  description = "Environment tag to add to the ec2 bastion host"
}
variable "role_tag" {
  type        = "string"
  description = "Tag of the role which will assume the ec2 instance"
  default     = "bastion"
}
