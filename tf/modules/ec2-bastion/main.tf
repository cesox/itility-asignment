module "security-group_ssh" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "2.11.0"

  vpc_id                      = "${var.vpc_id}"
  name                        = "ssh_to_bastion"
  ingress_cidr_blocks         = ["${var.allowed_ssh_in}"]
 
  tags = {
    Terraform =  "true"
    Environment = "${var.env_tag}"
  }

}

resource "aws_instance" "bastion" {
  ami                         = "${var.ami}"
  key_name                    = "${var.key_name}"
  instance_type               = "${var.instance_type}"
  associate_public_ip_address = "true"
  vpc_security_group_ids      = ["${module.security-group_ssh.this_security_group_id}"]
  subnet_id                   = "${var.subnet_id}"

  tags = {
    Terraform   = true
    Environment = "${var.env_tag}"
    Name        = "bastion-${var.env_tag}"
    Role        = "${var.role_tag}"
  }
}
# create security group to ssh to the priv instances
module "security-group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "2.11.0"

  vpc_id                      = "${var.vpc_id}"
  name                        = "ssh_bastion_to_hosts"
  
  tags = {
    Terraform   = "true"
    Environment = "${var.env_tag}" 
  }

}
# create security group rule to allow ssh from bastion to instances on private subnet
resource "aws_security_group_rule" "bastion_to_priv" {
  type                     = "ingress"
  from_port                = "22"
  to_port                  = "22"
  protocol                 = "tcp"
  security_group_id        = "${module.security-group.this_security_group_id}"
  source_security_group_id = "${module.security-group_ssh.this_security_group_id}"
}
