output "public_ip" {
  description = "Public ip of the bastion host"
  value       = "${aws_instance.bastion.public_ip}"
}
output "bastion_security_group_id" {
  description = "Id of the bastion security group"
  value       = "${module.security-group_ssh.this_security_group_id}"
}
output "bastion_to_host_security_group_id" {
  description = "Id of the security group allowing ssh from bastion to host"
  value       = "${module.security-group.this_security_group_id}"
}
