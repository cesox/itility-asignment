pipeline {
  agent any
  environment {
    AWS_CRED_ID           = "aws_personal"
    TF_DIR                = "./tf/"
    ANSIBLE_DIR           = "./ansible/"
    SCRIPTS_DIR           = "./scripts/"
    CRED_ID_SSH           = "deployer_key"
    AWS_ACCESS_KEY_ID     = credentials('personal_key_id')
    AWS_SECRET_ACCESS_KEY = credentials('personal_secret_aws')
    AWS_DEFAULT_REGION    = "us-east-1"
  }

  stages {
    stage('Stage - Run and Apply TF') {
      environment {
        TF_ENV = "stage"
      } 
      steps {
        dir("${env.TF_DIR}/${env.TF_ENV}/vpc") {
          sh 'terraform init'
          sh 'terraform refresh'
          sh 'terraform plan -out vpc.plan'
          sh 'terraform apply -input=false vpc.plan'
        }
        dir("${env.TF_DIR}/${env.TF_ENV}/lb-ec2") {
          sh 'terraform init'
          sh 'terraform refresh'
          sh 'terraform plan -out lb-ec2.plan'
          sh 'terraform apply -input=false lb-ec2.plan'
          sh 'terraform output -json > lb-ec2.json'
        }
        dir("${env.TF_DIR}/${env.TF_ENV}/aurora") {
          sh 'terraform init'
          sh 'terraform refresh'
          sh 'terraform plan -out aurora.plan'
          sh 'terraform apply -input=false aurora.plan'
          sh 'terraform output -json > aurora.json'
        }
      }
    }
    stage('Install/Update terraform-inventory') {
      environment {
        TF_INV_URL   = "https://github.com/adammck/terraform-inventory/releases/download/v0.7-pre/"
        TF_INVENTORY = "terraform-inventory_v0.7-pre_linux_amd64"
      }
      steps {
        dir("${env.ANSIBLE_DIR}") {
          sh "wget ${env.TF_INV_URL}${env.TF_INVENTORY}.zip"
          sh "unzip -o ${env.TF_INVENTORY}.zip" 
          sh "chmod u+x terraform-inventory"
          sh "rm -f ${env.TF_INVENTORY}.zip"
        }
      }
    }
    stage('Create dynamic SSH and ansible confs') {
      environment {
        TF_ENV        = "stage"
        PRIV_KEY_PATH = "../id_rsa"
      }
      steps {
        dir("${env.ANSIBLE_DIR}") {
          script {
            sh "chmod u+x .${env.SCRIPTS_DIR}/*"
            sh ".${env.SCRIPTS_DIR}/create_ssh_conf.sh .${env.TF_DIR}/${env.TF_ENV}/lb-ec2/lb-ec2.json ${env.PRIV_KEY_PATH}" 
            sh ".${env.SCRIPTS_DIR}/conf-playbook-from-tf.sh .${env.TF_DIR}/${env.TF_ENV}/aurora/aurora.json .${env.ANSIBLE_DIR}/playbook.yaml"
          }
        }
      }
    }
    stage('Deploy app into ec2') {
      environment {
        TF_STATE    = ".${env.TF_DIR}/stage/lb-ec2/terraform.tfstate"
        TF_KEY_NAME = "private_ip"
      }
      steps {
        dir("${env.ANSIBLE_DIR}") {
          sh 'ansible-galaxy install -r requirements.yml -p ./roles/'
          sh 'ansible-playbook --inventory-file=terraform-inventory playbook.yaml'
        }
      }
    }
    stage('Run dummy smoke test') {
      steps {
        dir("${env.SCRIPTS_DIR}") {
          sh "./dummy-test.sh .${env.TF_DIR}/stage/lb-ec2/lb-ec2.json"
        }
      }
    }
  }
  post {
    success {
      dir("${env.ANSIBLE_DIR}") {
        sh "rm -f playbook.yaml"
        sh "mv playbook.yaml.bkp playbook.yaml"
        sh "rm -f ansible.cfg"
        sh "rm -f ssh.config"
      }
    }
  }  
}
