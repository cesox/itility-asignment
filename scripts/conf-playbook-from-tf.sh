#!/bin/bash

database_output_path="$1"
playbook_path="$2"

# make backup of playbook
cp "$playbook_path" "$playbook_path.bkp"
sed -i "s/rds_username/$( jq '.rds_master_username.value' $database_output_path )/" "$playbook_path"
sed -i "s/rds_password/$( jq '.rds_master_password.value' $database_output_path )/" "$playbook_path"
sed -i "s/rds_database_name/$( jq '.rds_database_name.value' $database_output_path )/" "$playbook_path"
sed -i "s/rds_endpoint/$( jq '.rds_endpoint.value' $database_output_path )/" "$playbook_path"
