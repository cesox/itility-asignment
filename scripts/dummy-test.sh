#!/bin/bash
path_json="$1"
lb_endpoint=$( jq '.elb_dns_endpoint.value' $path_json )

count=`curl -s "$lb_endpoint" | grep -ci "redmine"`

if [[ "$count" != "0" ]]; then
  echo "Deploy succsesful"
  exit 0   
else
  echo "Site not deployed"
  exit 1
fi
