#!/bin/bash

database_output_path="$1"
priv_key_path="$2"
bastion_ip=$( jq '.bastion_public_ip.value' $database_output_path )

# create ssh.config file using the input recibed
cat > ssh.config <<EOF
Host bastion
    HostName               $bastion_ip 
    IdentityFile           $priv_key_path
    User                   ec2-user

Host 192.168.10.*
    User                   ec2-user
    ProxyCommand           ssh -q -W %h:%p bastion
    IdentityFile           $priv_key_path
EOF

# create ssh config for ansible connections
cat > ansible.cfg <<EOF
[ssh_connection]
ssh_args = -F ssh.config -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
EOF
